using NDesk.Options;
using Wikimedia;

public class Stubs
{
    private bool debugs = false;
    private bool force = false;
    private bool help = false;
    private int  max = 1;
    private bool verbose = false;
    private List<string> extras;

    private bool isSoftRedirect (ArticlePage article)
    {
        article.Load();
        bool isSoftRedirect = article.Exists("Wiktionary redirect") || article.Exists("Wikidata redirect");
        return isSoftRedirect;
    }

    private void ProcessStub(TalkPage talk)
    {
    	talk.Load ();
     
        // Find WikiProject banner shell
        var bannerShell = ProjectBannerShell.GetProjectBannerShell (talk);
        if (null == bannerShell)
        {
            Console.WriteLine("Could not find banner shell");
        }
        else
        {
            bannerShell.Class = "Stub";
        }

        if (force)
        {
            talk.Save("Automated stub rating");
        }

        if (verbose)
        {
            Console.WriteLine(talk);
        }
    }

    private void ProcessPage(ArticlePage article, TalkPage talk)
    {
        // Until someone tells me what to do with these
        if (isSoftRedirect(article))
        {
            Console.WriteLine("Soft redirect - skipped");
            return;
        }

        string prediction = article.Prediction();
        Console.WriteLine("Page: " + article.Title + " Prediction: " + prediction);
        if (prediction.Equals("Stub"))
        {
            ProcessStub(talk);
            max--;
        }
    }

    private void ProcessPage(string title)
    {
        var article = new ArticlePage(title);
        ProcessPage(article, article.Talk);
    }

    private void ProcessCategory(string category)
    {
    	Console.WriteLine ("Entered: " + category);
        var query = new Query(category, 10);
        do 
        {
      	  	var pages = query.Pages;
            foreach (var page in pages)
            {
                if (page.Namespace.Text.Equals ("Category"))
	        	{
                    ProcessCategory(page.TitleWithoutNamespace);
                } 
                else 
                {
                    if (page is TalkPage)
                    {
                        var talk = (TalkPage)page;
                        var article = talk.Article;
                        ProcessPage(article, talk);
                    }
             	}
        		if (max <= 0)
				{
					return;
                }
            }
        } 
        while (query.Continue);
        Console.WriteLine ("Exited: " + category);
    }

    private void Process()
    {
        Cred.Instance.Showtime("started");
        if (extras.Any())
        {
            foreach (var title in extras)
            {
                ProcessPage(title);
            }
        }
        else
        { 
            ProcessCategory("Unassessed articles");
        }
        Cred.Instance.Showtime("done");
    }

    static int ParseIntOrDie(string someStr)
    {
        if (int.TryParse(someStr, out int result))
        {
            return result;
        }
        throw new Exception("Could not convert '" + someStr + "' to an integer");
    }

    private static void ShowHelp(OptionSet options)
    {
        Console.WriteLine("Usage: Stubs [OPTIONS]+");
        Console.WriteLine("Assess unassessed articles and tag the stubs.");
        Console.WriteLine();
        Console.WriteLine("Options:");
        options.WriteOptionDescriptions(Console.Out);
        Environment.Exit(0);
    }

    List<string> Options(string[] args)
    {
        var optionSet = new OptionSet() {
            { "d|debug",     "debugging",    v => debugs  = v != null },
            { "f|force",     "update page",  v => force   = v != null },
            { "h|?|help",    "display help", v => help    = v != null },
            { "n|max=",      "number of pages to process",  v => max = ParseIntOrDie(v) },
            { "v|verbose",   "vebosity",     v => verbose = v != null },
        };

        extras = optionSet.Parse(args);

        if (help)
        {
            ShowHelp(optionSet);
        }

        return extras;
    }

    private Stubs(string[] args)
    {
        extras = Options(args);
        Debug.On = debugs;
        Debug.Offline = true;
     }

    static public void Main(string[] args)
    {
        var stubs = new Stubs(args);
        stubs.Process();
    }
}